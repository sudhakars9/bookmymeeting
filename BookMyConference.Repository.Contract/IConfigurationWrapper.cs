﻿//-----------------------------------------------------------------------
// <copyright file="IConfigurationWrapper.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Contract
{
    /// <summary>
    /// Interface for configuration helper.
    /// </summary>
    public interface IConfigurationWrapper
    {
        /// <summary>
        /// Returns value from configuration file.
        /// </summary>
        /// <param name="key">Key name.</param>
        /// <returns>Returns Value.</returns>
        string GetAppSettingsValue(string key);

        /// <summary>
        /// Get Connection String.
        /// </summary>
        /// <param name="key">Key name.</param>
        /// <returns>Returns connection string</returns>
        string GetConnectionString(string key);
    }
}
