﻿//-----------------------------------------------------------------------
// <copyright file="IDatabaseConnectionManager.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Contract
{
    /// <summary>
    /// Interface for Database Connection.
    /// </summary>
    public interface IDatabaseConnectionManager
    {
        /// <summary>
        /// Connects to database and returns handler.
        /// </summary>
        /// <returns>Returns database handler.</returns>
        dynamic ConnectToDatabase();
    }
}
