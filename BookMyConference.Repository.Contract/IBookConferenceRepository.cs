﻿//-----------------------------------------------------------------------
// <copyright file="IBookConferenceRepository.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Contract
{
    using System.Collections.Generic;
    using Entity;

    /// <summary>
    /// Interface IBookConferenceRepository
    /// </summary>
    public interface IBookConferenceRepository
    {
        /// <summary>
        /// Saves the meeting information.
        /// </summary>
        /// <param name="bookingInfo">The booking information.</param>
        /// <returns>Will return a boolean value for the Data base operation.</returns>
        bool SaveMeetingInfo(ConferenceRoomDetails bookingInfo);

        /// <summary>
        /// Checks the exists meeting information.
        /// </summary>
        /// <param name="bookingInfo">The booking information.</param>
        /// <returns>Will return a boolean value about </returns>
        bool CheckExistsMeetingInfo(ConferenceRoomDetails bookingInfo);

        /// <summary>
        /// Retrieves the booked meeting information.
        /// </summary>
        /// <param name="bookingRoomLocation">The booking room location.</param>
        /// <returns>will return the history of booking information.</returns>
        IEnumerable<ConferenceRoomDetails> RetriveBookedMeetingInfo(string bookingRoomLocation);

        IEnumerable<ConferenceRoom> GetAvailableRoomsList(System.DateTime startTime, System.DateTime endTime);
    }
}
