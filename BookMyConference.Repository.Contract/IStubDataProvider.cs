﻿//-----------------------------------------------------------------------
// <copyright file="IStubDataProvider.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Contract
{
    using System.Collections.Generic;
    using Entity;

    /// <summary>
    /// Interface for Conference Room Info.
    /// </summary>
    public interface IStubDataProvider
    {
        /// <summary>
        /// Gets the conference room list.
        /// </summary>
        /// <returns>Will return the collection of Conference rooms.</returns>
        IEnumerable<ConferenceRoom> GetConferenceRoomList();
    }
}
