﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceControllerTest.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.WebApi.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Hosting;
    using Entity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Repository.Contract;
    using WebApi.Controllers;

    /// <summary>
    /// Tests Conference Controller.
    /// </summary>
    [TestClass]
    public class ConferenceControllerTest
    {
        /// <summary>
        /// Tests Test_Get_Conference_Room_List.
        /// </summary>
        [TestMethod]
        public void Test_Get_Conference_Room_List()
        {
            ////Setup
            var provider = new Mock<IStubDataProvider>();
            var conferenceRepository = new Mock<IBookConferenceRepository>();
            provider.Setup(x => x.GetConferenceRoomList()).Returns(new List<ConferenceRoom>());
            ////Act
            ConferenceController controller = new ConferenceController(provider.Object, conferenceRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            var result = controller.GetConferenceRoomList().StatusCode;
            ////Assert.
            Assert.AreEqual(result, HttpStatusCode.OK);
        }

        /// <summary>
        /// Tests Test_Post_Save_Conference_Data.
        /// </summary>
        [TestMethod]
        public void Test_Post_Save_Conference_Data()
        {
            ////Setup
            var provider = new Mock<IStubDataProvider>();
            var conferenceRepository = new Mock<IBookConferenceRepository>();
            provider.Setup(x => x.GetConferenceRoomList()).Returns(new List<ConferenceRoom>());
            conferenceRepository.Setup(x => x.SaveMeetingInfo(null));
            ////Act
            ConferenceController controller = new ConferenceController(provider.Object, conferenceRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            var result = controller.PostSaveConferenceData(new ConferenceRoomDetails()).StatusCode;
            ////Assert.
            Assert.AreEqual(result, HttpStatusCode.OK);
        }

        /// <summary>
        /// Tests TestGet_Check_Room_Availability.
        /// </summary>
        [TestMethod]
        public void TestGet_Check_Room_Availability()
        {
            ////Setup
            var provider = new Mock<IStubDataProvider>();
            var conferenceRepository = new Mock<IBookConferenceRepository>();
            var searchEntity = new ConferenceRoomDetails();
            searchEntity.StartDateTime = DateTime.Now;
            searchEntity.EndDateTime = DateTime.Now;
            searchEntity.BookingRoomLocation = string.Empty;
            conferenceRepository.Setup(x => x.CheckExistsMeetingInfo(searchEntity)).Returns(true);
            ////Act
            ConferenceController controller = new ConferenceController(provider.Object, conferenceRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            var result = controller.GetCheckRoomAvailability(DateTime.Now, DateTime.Now, string.Empty);
            ////Assert.
            Assert.AreEqual(result.IsSuccessStatusCode, true);
        }

        /// <summary>
        /// Tests Test_Get_Booked_Meeting_Information.
        /// </summary>
        [TestMethod]
        public void Test_Get_Booked_Meeting_Information()
        {
            ////Setup
            var provider = new Mock<IStubDataProvider>();
            var conferenceRepository = new Mock<IBookConferenceRepository>();
            conferenceRepository.Setup(x => x.RetriveBookedMeetingInfo(null)).Returns(new List<ConferenceRoomDetails>());
            ////Act
            ConferenceController controller = new ConferenceController(provider.Object, conferenceRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            var result = controller.GetBookedMeetingInformation(null).StatusCode;
            ////Assert.
            Assert.AreEqual(result, HttpStatusCode.OK);
        }
    }
}