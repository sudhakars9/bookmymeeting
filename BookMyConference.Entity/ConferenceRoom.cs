﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceRoom.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace BookMyConference.Entity
{
    /// <summary>
    /// Class for Conference Room Information.
    /// </summary>
    public class ConferenceRoom
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the projector.
        /// </summary>
        /// <value>
        /// The projector.
        /// </value>
        public bool Projector { get; set; }

        /// <summary>
        /// Gets or sets the lync availability.
        /// </summary>
        /// <value>
        /// The Lync.
        /// </value>
        public bool Lync { get; set; }
    }
}