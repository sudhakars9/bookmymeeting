﻿//-----------------------------------------------------------------------
// <copyright file="Employee.cs" company="TCS">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------
namespace BookMyConference.Entity
{
    /// <summary>
    /// This is employee class
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Gets or sets the name of the employee.
        /// </summary>
        /// <value>
        /// The name of the employee.
        /// </value>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the mobile number.
        /// </summary>
        /// <value>
        /// The mobile number.
        /// </value>
        public string MobileNumber { get; set; }

        /// <summary>
        /// Gets or sets the email identifier.
        /// </summary>
        /// <value>
        /// The email identifier.
        /// </value>
        public string EmailId { get; set; }
    }
}
