﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceRoomDetails.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Entity
{
    using System;
    using MongoDB.Bson.Serialization.Attributes;

    /// <summary>
    /// Entity of Conference Room Details.
    /// </summary>
    [BsonIgnoreExtraElements]
    public class ConferenceRoomDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConferenceRoomDetails"/> class.
        /// </summary>
        public ConferenceRoomDetails()
        {
            Employee = new Employee();
        }

        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        /// <value>
        /// The start date time.
        /// </value>
        [BsonDateTimeOptions(Kind = DateTimeKind.Unspecified)]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        /// <value>
        /// The end date time.
        /// </value>
        [BsonDateTimeOptions(Kind = DateTimeKind.Unspecified)]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the type of booking.
        /// </summary>
        /// <value>
        /// The type of booking.
        /// </value>
        public string TypeOfBooking { get; set; }

        /// <summary>
        /// Gets or sets the booking room location.
        /// </summary>
        /// <value>
        /// The booking room location.
        /// </value>
        public string BookingRoomLocation { get; set; }

        /// <summary>
        /// Gets or sets the employee.
        /// </summary>
        /// <value>
        /// The employee.
        /// </value>
        public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the purpose of booking.
        /// </summary>
        /// <value>
        /// The purpose of booking.
        /// </value>
        public string PurposeOfBooking { get; set; }

        /// <summary>
        /// Gets or sets the duration of the meeting.
        /// </summary>
        /// <value>
        /// The duration of the meeting.
        /// </value>
        public string MeetingDurationTime { get; set; }

        /// <summary>
        /// Gets or sets the meeting days.
        /// </summary>
        /// <value>
        /// The meeting days.
        /// </value>
        public string MeetingDurationInDate { get; set; }
    }
}
