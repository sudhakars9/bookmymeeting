//-----------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using BookMyConference.Web;

[assembly: WebActivator.PreApplicationStartMethod(typeof(StructuremapMvc), "Start")]

namespace BookMyConference.Web
{
    using System.Web.Http;
    using System.Web.Mvc;
    using DependencyResolution;
    using StructureMap;

    /// <summary>
    /// Class for structure map.
    /// </summary>
    public static class StructuremapMvc
    {
        /// <summary>
        /// Starts this instance.
        /// </summary>
        public static void Start()
        {
            var container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}