﻿//-----------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace BookMyConference.Web
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Class for config routing.
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registers the routes.
        /// </summary>
        /// <param name="routes">The routes.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Conference", action = "Index", id = UrlParameter.Optional });
        }
    }
}