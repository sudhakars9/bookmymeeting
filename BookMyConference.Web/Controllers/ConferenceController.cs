﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceController.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Web.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Controller class.
    /// </summary>
    public class ConferenceController : Controller
    {
        //// GET: /Conference/

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>will return the action result.</returns>
        public ActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Views the calendar.
        /// </summary>
        /// <returns>will return the action result.</returns>
        public ActionResult ViewCalendar()
        {
            return this.View();
        }
    }
}
