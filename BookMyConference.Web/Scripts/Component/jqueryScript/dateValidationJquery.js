﻿    $(document).ready(function () {
        $("#spanToDate").css("pointer-events", "none");
        $("#spanFromDate").css("pointer-events", "none");
        $("#errorMsgId").hide();
        $('#MainId').delay(1).show();
    });
    $(window).load(function () {
        var hoursToDisable = angular.element('#RoomBookingController').scope().dateConfig.hoursDisabled;
        var daysToShowForBooking ="+" + angular.element('#RoomBookingController')
                                        .scope().dateConfig.numberOfDaysToAllowBooking +"d";

        $('#from_date').datetimepicker({
            weekStart: 0,
            todayBtn: 1,
            autoclose: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "dd-mm-yyyy",
            startDate: '-0d',
            endDate: daysToShowForBooking,
            daysOfWeekDisabled: '0,6',
            todayHighlight: true
        });
        $('#from_time').datetimepicker({
            weekStart: 1,
            todayBtn: false,
            autoclose: 1,
            todayHighlight: false,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            formatViewType: "time",
            hoursDisabled: hoursToDisable
        });
        $('#to_date').datetimepicker({
            weekStart: 0,
            todayBtn: 1,
            autoclose: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "dd-mm-yyyy",
            startDate: '-0d',
            endDate: daysToShowForBooking,
            daysOfWeekDisabled: '0,6',
            todayHighlight: true
        });
        $('#to_time').datetimepicker({
            weekStart: 1,
            todayBtn: false,
            autoclose: 1,
            todayHighlight: false,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            formatViewType: "time",
            hoursDisabled: hoursToDisable
        });
        
    });
    
$("#from_time").on("change", function () {
    bindControls();
    return angular.element('#RoomBookingController').scope().onDateChange();      
});
$("#from_date").on("change", function () {
    bindControls();
    return angular.element('#RoomBookingController').scope().onDateChange();
});
$("#to_time").on("change", function () {
    bindControls();
    return angular.element('#RoomBookingController').scope().onDateChange();
});
$("#to_date").on("change", function () {
    bindControls();
    return angular.element('#RoomBookingController').scope().onDateChange();        
});

$("#checkIsRoomAvailable").on("click", function () {
    bindControls();
    angular.element('#RoomBookingController').scope().roomValidateclicked = true;
    return angular.element('#RoomBookingController').scope().validateRoomAndTime();
});

var bindControls = function () {
    var fromDate = $('#FromDate').val().split('-');
    var oDate = new Date(fromDate[2], fromDate[1] - 1, fromDate[0], 0, 0, 0, 0);
    angular.element('#RoomBookingController').scope().Conference.FromDate = oDate;
        
    var fromTime = $('#FromTime').val().split(':');
    var oDtime = new Date("2014", "05", "29", fromTime[0], fromTime[1], 0, 0);
    angular.element('#RoomBookingController').scope().Conference.FromTime = oDtime;

    var toDate = $('#ToDate').val().split('-');
    var tooDate = new Date(toDate[2], toDate[1] - 1, toDate[0], 0, 0, 0, 0);
    angular.element('#RoomBookingController').scope().Conference.ToDate = tooDate;

    var toTime = $('#ToTime').val().split(':');
    var tootime = new Date("2014", "05", "29", toTime[0], toTime[1], 0, 0);
    angular.element('#RoomBookingController').scope().Conference.ToTime = tootime;

};

$('#todayId').on("change", function () {
    $("#spanToDate").css("pointer-events", "none");
    $("#spanFromDate").css("pointer-events", "none");
    angular.element('#RoomBookingController').scope().radioChange(true);
});
$('#futureId').on("change", function () {
    $("#spanToDate").css("pointer-events", "");
    $("#spanFromDate").css("pointer-events", "");
    return angular.element('#RoomBookingController').scope().radioChange(false);
});
$('#searchBookId').on("change", function () {
    $("#spanToDate").css("pointer-events", "");
    $("#spanFromDate").css("pointer-events", "");        
    return angular.element('#RoomBookingController').scope().radioChange(false);
});

$("#checkAvailableRooms").on("click", function () {
    bindControls();        
    return angular.element('#RoomBookingController').scope().getAvailableRooms();
});
