﻿angular.module('MeetingRoomSharedFactoryModule', []).factory('sharedFactory', function ($rootScope, webApiService) {
    var factory = {};
    factory.roomHistoryData = [];
    factory.broadCastHistoryData = function () {
        $rootScope.$broadcast('broadCastHistoryDataHandler');
    }
    return factory;
});