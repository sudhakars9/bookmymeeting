﻿angular.module('BusinessService', []).service('businessSrvice', function () {
    this.onLoadSetDateandTime = function (scope) {
        var now = new Date();
        var toDateTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes() +scope.dateConfig.defaultMeetingDurationMinutes, 0);
        var currentDate = now.getDate() + "-" + now.getMonth()+ 1 + "-" + now.getFullYear();
        var currentTime = now.getHours() + ":" + now.getMinutes();
        var toTime = toDateTime.getHours() + ":" + toDateTime.getMinutes();
        
        scope.Conference.FromDateInit = currentDate;
        scope.Conference.ToDateInit = currentDate;
        scope.Conference.FromTimeInit = currentTime;
        scope.Conference.ToTimeInit = toTime;

        scope.Conference.FromDate = currentDate;
        scope.Conference.ToDate = currentDate;
        scope.Conference.FromTime = currentTime;
        scope.Conference.ToTime = toTime;
        
        scope.isDateInValid = false;
    }

    this.validateFromToDates = function (scopeValue) {
        var startDateTime = this.bindDateAndTime(scopeValue.Conference.FromDate, scopeValue.Conference.FromTime);
        var endDateTime = this.bindDateAndTime(scopeValue.Conference.ToDate, scopeValue.Conference.ToTime);
        var differenceBetweenTowDates = startDateTime.getTime() - endDateTime.getTime();
        var currentDate = new Date();
        var currentTimeValue = new Date(currentDate.getFullYear(),currentDate.getMonth(), currentDate.getDate()).getTime();
        
        if (startDateTime.getTime() < currentTimeValue){
            return true;
        }
        else if (endDateTime.getTime() < currentTimeValue) {
            return true;
        }
        else if (differenceBetweenTowDates > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    this.bindDateAndTime = function (date, time) {       
        var fullDateTime = new Date(date.getFullYear(), date.getMonth(),
                           date.getDate(), time.getHours(), time.getMinutes(), 0, 0);
        return fullDateTime;
    }

    this.convertBasicSearchDataToEntity = function (scopeValue) {
        var searchDataEntity = {};
        searchDataEntity.StartDateTime = this.convertToShortDateTime(scopeValue.Conference.FromDate, scopeValue.Conference.FromTime);
        searchDataEntity.EndDateTime = this.convertToShortDateTime(scopeValue.Conference.ToDate, scopeValue.Conference.ToTime);
        searchDataEntity.SelectedRoomName = scopeValue.Conference.SelectedRoomName;

        return searchDataEntity;
    }

    this.convertScopeDataToRoomBusinessEntity = function (conference) {

        var conferenceRoomDetails = {};
        conferenceRoomDetails.Employee = {};
        // mapper Start datetime
        conferenceRoomDetails.StartDateTime = this.convertToShortDateTime(conference.FromDate, conference.FromTime);
        // mapper end datetime
        conferenceRoomDetails.EndDateTime = this.convertToShortDateTime(conference.ToDate, conference.ToTime);
        // mapper BookingRoomLocation
        conferenceRoomDetails.BookingRoomLocation = conference.SelectedRoomName;
        // mapper TypeOfBooking
        conferenceRoomDetails.TypeOfBooking = "Daily";
        // mapper EmployeeName
        conferenceRoomDetails.Employee.EmployeeName = conference.ContactPerson;
        // mapper MobileNumber
        conferenceRoomDetails.Employee.MobileNumber = conference.MobileNumber;
        // mapper EmailId
        conferenceRoomDetails.Employee.EmailId = conference.EmailId;
        // mapper PurposeOfBooking
        conferenceRoomDetails.PurposeOfBooking = conference.Description;

        return conferenceRoomDetails;
    }


    this.convertToShortDateTime = function (date, time) {
        var dateValue = ('0' + (date.getMonth() + 1)).slice(-2) + "/" + date.getDate() + "/" + date.getFullYear() + " " + time.getHours() + ":" + time.getMinutes();
        return dateValue;
    }   
});