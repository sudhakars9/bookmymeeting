﻿angular.module('WebApiService', []).service('webApiService', function ($http, webApiUrl) {
    this.getAllConferenceRoomList = function () {
        return $http.get(webApiUrl + "GetConferenceRoomList");
    }

    this.saveData = function (conferenceData) {

        var jsonData = JSON.stringify(conferenceData);
        var request = $http({
            method: "post",
            headers: { 'Content-Type': 'application/json' },
            url: webApiUrl + 'PostCheckRoomAvailability',
            data: jsonData
        });
        return request;
    }

    this.checkIsRoomAvailable = function (inputData) {
        var urlWithQueryString = "?startTime=" + inputData.StartDateTime + "&endTime=" + inputData.EndDateTime + "&conferenceRoom=" + inputData.SelectedRoomName;
        var request = $http.get(webApiUrl + "GetCheckRoomAvailability" + urlWithQueryString);
        return request;
    }

    this.getRoomHistoryData = function (roomName) {        
        var request = $http.get(webApiUrl + "GetBookedMeetingInformation?conferenceRoom=" + roomName);
        return request;
    }

    this.getAvailableRooms = function (inputData) {
        var urlWithQueryString = "?startTime=" + inputData.StartDateTime + "&endTime=" + inputData.EndDateTime;
        var request = $http.get(webApiUrl + "GetAvailableRooms" + urlWithQueryString);
        return request;
    }

});
