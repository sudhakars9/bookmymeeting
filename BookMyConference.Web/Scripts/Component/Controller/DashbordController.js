﻿angular.module("DashbordControler", ['ui.bootstrap', 'ngGrid']).controller('GridCtrl', function ($scope, $http, sharedFactory) {

    var recordsPerPage = 10;
    var templateWithTooltip = '<div><a tooltip="{{row.getProperty(col.field)}}" tooltip-append-to-body="true" tooltip-placement="right" >{{row.getProperty(col.field)}}</a></div>';
    var showProjectorCheckBox = '<div class="ngSelectionCell"><input class="ngSelectionCheckbox" type="checkbox" ng-disabled="true" ng-model="row.entity.Projector" /></div>';
    var showLyncCheckBox = '<div class="ngSelectionCell"><input class="ngSelectionCheckbox" type="checkbox" ng-disabled="true" ng-model="row.entity.Lync" /></div>';
    var bookConferenceLink = '<div><a style="cursor:pointer" ng-click="BookConferenceRoomForm(row.entity.Name)">&nbsp;Book Now</a></div>';

    var customFooterSelection = "<div ng-grid-footer=\"\" class=\"ng-scope\"><div ng-show=\"showFooter\" class=\"ngFooterPanel ng-scope\" ng-class=\"{'ui-widget-content': jqueryUITheme, 'ui-corner-bottom': jqueryUITheme}\" ng-style=\"footerStyle()\" style=\"width: 600px; height: 55px;\">" +
        "<div class=\"ngPagerContainer ngNoMultiSelect\" style=\"float: right; margin-top: 10px;\" ng-show=\"enablePaging\" ng-class=\"{'ngNoMultiSelect': !multiSelect}\">" +
        " <div style=\"float:left; margin-right: 10px; line-height:25px;\" class=\"ngPagerControl\">" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageToFirst()\" ng-disabled=\"cantPageBackward()\" title=\"{{i18n.ngPagerFirstTitle}}\"><div class=\"ngPagerFirstTriangleCustom\"><div class=\"ngPagerFirstBarCustom\"></div></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageBackward()\" ng-disabled=\"cantPageBackward()\" title=\"Previous Page\"><div class=\"ngPagerFirstTriangleCustom ngPagerPrevTriangleCustom\"></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageForward()\" ng-disabled=\"cantPageForward()\" title=\"Next Page\" disabled=\"disabled\"><div class=\"ngPagerLastTriangleCustom ngPagerNextTriangleCustom\"></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageToLast()\" ng-disabled=\"cantPageToLast()\" title=\"{{i18n.ngPagerLastTitle}}\"><div class=\"ngPagerLastTriangleCustom\"><div class=\"ngPagerLastBarCustom\"></div></div></button>" +
        "  </div>" + "</div>" + "</div></div>";

    $scope.datalist = [];

    var setFieldsToGrid = function() {
        if ($scope.searchAndRoomValidateclicked) {
           // col definition
            $scope.colDef = [
                { field: "Name", displayName: "Conference Room", cellTemplate: templateWithTooltip, width: '40%' },
                { field: "Projector", displayName: "Projector", cellTemplate: showProjectorCheckBox, width: '15%' },
                { field: "Lync", displayName: "Lync", cellTemplate: showLyncCheckBox, width: '15%' },
                { cellTemplate: bookConferenceLink, width: '30%' }
            ];
        } else {

            // col definition
            $scope.colDef = [
                { field: "BookingRoomLocation", displayName: "Conference Room", cellTemplate: templateWithTooltip, width: '25%' },
                {
                    field: "MeetingDurationInDate",
                    displayName: "Booking Period",
                    cellTemplate: templateWithTooltip,
                    width: '30%'
                },
                { field: "MeetingDurationTime", displayName: "Duration", cellTemplate: templateWithTooltip, width: '10%' },
                { field: "Employee.EmailId", displayName: "Booked By", cellTemplate: templateWithTooltip, width: '15%' },
                { field: "PurposeOfBooking", displayName: "Purpose", cellTemplate: templateWithTooltip, width: '20%' }
            ];
        }
    }

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };

    $scope.pagingOptions = {
        pageSizes: [10, 20],
        pageSize: recordsPerPage,
        currentPage: 1
    };

    $scope.$on('broadCastHistoryDataHandler', function () {
        setFieldsToGrid();
        var data = sharedFactory.roomHistoryData;
        var page = $scope.pagingOptions.currentPage;
        var pageSize = $scope.pagingOptions.pageSize;
        $scope.datalist = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.OrgList = data;
        $scope.totalServerItems = data.length;
    });

    $scope.setPagingData = function (page, pageSize) {
        var data = $scope.OrgList;
        if (data != null)
        {
            var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
            $scope.datalist = pagedData;
            $scope.totalServerItems = data.length;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }        
    };

    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            $scope.setPagingData(page, pageSize);
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    
    $scope.$watch('pagingOptions', function () {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }, true);
    $scope.$watch('filterOptions', function () {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }, true);

    $scope.gridOptions = {
        data: 'datalist',
        enableRowSelection : false,
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        footerRowHeight: 40,
        footerTemplate : customFooterSelection,
        columnDefs: 'colDef'
    };

});