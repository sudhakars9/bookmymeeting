﻿angular.module("SearchData", ['ui.bootstrap', 'ngGrid']).controller('SearchCtrl', function ($scope, $http, sharedFactory) {

    $scope.searchList = [{ BookingRoomLocation: "Moroni", Projector: true, Lync: true, Book: "Book" },
                 { BookingRoomLocation: "Tiancum", Projector: true, Lync: true, Book: "Book" },
                 { BookingRoomLocation: "Jacob", Projector: false, Lync: false, Book: "Book" },
                 { BookingRoomLocation: "Nephi", Projector: false, Lync: false, Book: "Book" },
                 { BookingRoomLocation: "Enos", Projector: true, Lync: false, Book: "Book" }];

    $scope.OrgList = $scope.searchList;

    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };

    $scope.totalServerItems = 0;

    $scope.pagingOptions = {
        pageSizes: [10, 20],
        pageSize: 10,
        currentPage: 1
    };

    $scope.setPagingData = function (page, pageSize) {
        var data = $scope.OrgList;
        if (data != null) {
            var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
            $scope.searchList = pagedData;
            $scope.totalServerItems = data.length;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }
    };

    $scope.getPagedDataAsync = function (pageSize, page) {
        setTimeout(function () {
            $scope.setPagingData(page, pageSize);
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }, true);

    $scope.BookConferenceRoomForm = function (selectedVal) {
        alert(selectedVal);
    };

    var templateWithTooltip = '<div><a tooltip="{{row.getProperty(col.field)}}" tooltip-append-to-body="true" tooltip-placement="left" >{{row.getProperty(col.field)}}</a></div>';
    var showProjectorCheckBox = '<div class="ngSelectionCell"><input class="ngSelectionCheckbox" type="checkbox" ng-disabled="true" ng-model="row.entity.Projector" /></div>';
    var showLyncCheckBox = '<div class="ngSelectionCell"><input class="ngSelectionCheckbox" type="checkbox" ng-disabled="true" ng-model="row.entity.Lync" /></div>';
    var bookConferenceLink = '<div><a ng-click="BookConferenceRoomForm(row.entity.BookingRoomLocation)">{{row.getProperty(col.field)}}</a></div>';

    var customFooterSelection = "<div ng-grid-footer=\"\" class=\"ng-scope\"><div ng-show=\"showFooter\" class=\"ngFooterPanel ng-scope\" ng-class=\"{'ui-widget-content': jqueryUITheme, 'ui-corner-bottom': jqueryUITheme}\" ng-style=\"footerStyle()\" style=\"width: 600px; height: 55px;\">" +
        "<div class=\"ngPagerContainer ngNoMultiSelect\" style=\"float: right; margin-top: 10px;\" ng-show=\"enablePaging\" ng-class=\"{'ngNoMultiSelect': !multiSelect}\">" +
        " <div style=\"float:left; margin-right: 10px; line-height:25px;\" class=\"ngPagerControl\">" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageToFirst()\" ng-disabled=\"cantPageBackward()\" title=\"{{i18n.ngPagerFirstTitle}}\"><div class=\"ngPagerFirstTriangle\"><div class=\"ngPagerFirstBar\"></div></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageBackward()\" ng-disabled=\"cantPageBackward()\" title=\"Previous Page\"><div class=\"ngPagerFirstTriangle ngPagerPrevTriangle\"></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageForward()\" ng-disabled=\"cantPageForward()\" title=\"Next Page\" disabled=\"disabled\"><div class=\"ngPagerLastTriangle ngPagerNextTriangle\"></div></button>" +
        "<button type=\"button\" class=\"ngPagerButtonCustom\" ng-click=\"pageToLast()\" ng-disabled=\"cantPageToLast()\" title=\"{{i18n.ngPagerLastTitle}}\"><div class=\"ngPagerLastTriangle\"><div class=\"ngPagerLastBar\"></div></div></button>" +
        "  </div>" + "</div>" + "</div></div>";

    $scope.gridOptions = {
        data: 'searchList',
        enablePaging: true,
        showFooter: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        footerRowHeight: 40,
        footerTemplate: customFooterSelection,
        columnDefs: [
            { field: "BookingRoomLocation", displayName: "Conference Room", cellTemplate: templateWithTooltip, width: '50%' },
            { field: "Projector", displayName: "Projector", cellTemplate: showProjectorCheckBox, width: '15%' },
            { field: "Lync", displayName: "Lync", cellTemplate: showLyncCheckBox, width: '15%' },
            { field: "Book", displayName: "Book", cellTemplate: bookConferenceLink, width: '20%' }
        ]
    };

});