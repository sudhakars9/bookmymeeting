﻿angular.module('BookingFormControllerModule', []).controller('RoomBookingController', function ($scope, webApiService, businessSrvice, sharedFactory, dateConfig) {
    // getting 
    $scope.dateConfig = dateConfig;
    $scope.Conference = {};
    // Onload Set current Date and Time
    businessSrvice.onLoadSetDateandTime($scope);

    $scope.conferenceRoomList = [];
    webApiService.getAllConferenceRoomList().success(function (data) { $scope.conferenceRoomList = data; });

    $scope.validateRoomAndTime = function () {
        $scope.BookedRoomName = "";
        $scope.isRoomAvailable = null;
        var inputData = businessSrvice.convertBasicSearchDataToEntity($scope);
        webApiService.checkIsRoomAvailable(inputData)
        .success(function(data) {
            $scope.isRoomAvailable = data;
                if (!data) {
                    var roomName = $scope.Conference.SelectedRoomName;
                    if (roomName != null) {
                        webApiService.getRoomHistoryData(roomName)
                            .success(function (historyData) {
                                sharedFactory.roomHistoryData = historyData;
                                sharedFactory.broadCastHistoryData();
                            })
                            .error(function() {});
                    }
                }
            })
        .error(function () { });
    };

    $scope.conferenceRoomChanged = function () {
        $scope.roomValidateclicked = false;
    }

    function setDefaultFormData(setCurrentDate) {
        $scope.submitted = false;
        $scope.roomValidateclicked = false;
        $scope.searchAndRoomValidateclicked = false;
        $scope.isRoomSelectedFormAvailableRooms = false;
        $scope.roomsAreAvailable = false;
        if (setCurrentDate) {
            $scope.Conference = {};
            businessSrvice.onLoadSetDateandTime($scope);
        }
    }

    function setBookingSuccessMessage() {
        $scope.BookedFromDate = businessSrvice.bindDateAndTime($scope.Conference.FromDate, $scope.Conference.FromTime).toLocaleString();
        $scope.BookedToDate = businessSrvice.bindDateAndTime($scope.Conference.ToDate, $scope.Conference.ToTime).toLocaleString();
        $scope.BookedRoomName = $scope.Conference.SelectedRoomName;
    }

    $scope.bookRoom = function (value,event) {
        
        if ($scope.isDateInValid) {
            event.preventDefault();
        }
        else {
            var inputBookingData = businessSrvice.convertScopeDataToRoomBusinessEntity(value);
            var saveSataus;
            webApiService.saveData(inputBookingData).success(function (data) { saveSataus = data; });
            setBookingSuccessMessage();
            setDefaultFormData(true);
        }
    };

    $scope.onDateChange = function () {
        $scope.$apply(function () {
            $scope.isDateInValid = businessSrvice.validateFromToDates($scope);
            if ($scope.isRoomAvailable) {
                $scope.roomValidateclicked = false;
            }
            $scope.roomsAreAvailable = false;
        });
    }
    $scope.radioChange = function (setCurrentDate) {
        $scope.$apply(function () {
            $scope.BookedRoomName = "";
            setDefaultFormData(setCurrentDate);
        });
    }

    $scope.getAvailableRooms = function () {
        var searchCriteria = businessSrvice.convertBasicSearchDataToEntity($scope);
        webApiService.getAvailableRooms(searchCriteria)
        .success(function (data) {
            if (data != null && data.length > 0)
            {
                $scope.roomsAreAvailable = true;
                $scope.searchAndRoomValidateclicked = true;
                $scope.isRoomAvailable = false;
                sharedFactory.roomHistoryData = data;
                sharedFactory.broadCastHistoryData();
            } else {
                sharedFactory.roomHistoryData = data;
                $scope.roomsAreAvailable = false;
                $scope.searchAndRoomValidateclicked = true;
            }
            }) 
        .error(function () { });
    }

    $scope.BookConferenceRoomForm = function (selectedVal) {
        $scope.isRoomSelectedFormAvailableRooms = true;
        $scope.isRoomAvailable = true;
        $scope.searchAndRoomValidateclicked = false;
        $scope.Conference.SelectedRoomName = selectedVal;

    };
});
