﻿calendarApp
.constant('webApiUrl', 'http://localhost:61950/api/Conference/') // web api url config
    //date and time configurable values
.constant('dateConfig',
{
    hoursDisabled: '0,1,2,3,4,5,6,7,8,10,21,22,23',
    defaultMeetingDurationMinutes: 30,
    numberOfDaysToAllowBooking:15
});