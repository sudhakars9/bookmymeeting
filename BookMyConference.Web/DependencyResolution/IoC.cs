//-----------------------------------------------------------------------
// <copyright file="IoC.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace BookMyConference.Web.DependencyResolution 
{
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Class for Structure map IoC.
    /// </summary>
    public static class IoC 
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns>Will hold the Object Instances.</returns>
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
            ////                x.For<IExample>().Use<Example>();
                        });
            return ObjectFactory.Container;
        }
    }
}