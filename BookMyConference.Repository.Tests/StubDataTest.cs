﻿//-----------------------------------------------------------------------
// <copyright file="StubDataTest.cs" company="Aviva">
//    Copyright (c) Aviva. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Tests
{
    using System;
    using System.IO;
    using System.Linq;
    using Contract;
    using Moq;
    using NUnit.Core;
    using NUnit.Framework;

    /// <summary>
    /// Test Class for StubData
    /// </summary>
    [TestFixture]
    public class StubDataTest
    {
        /// <summary>
        /// Tests Test_Get_Conference_Room_List operation.
        /// </summary>
        [Test]
        public void Test_Get_Conference_Room_List()
        {
            ////Set up.
            var confighHelper = new Mock<IConfigurationWrapper>();
            AppDomain.CurrentDomain.AppendPrivatePath(AppDomain.CurrentDomain.BaseDirectory);
            var stubData = new StubData(confighHelper.Object);
            confighHelper.Setup(x => x.GetAppSettingsValue("Conference_Room_Data_Key"))
                .Returns(Path.Combine(Environment.CurrentDirectory, "StubData\\ConferenceRooms.xml"));

            ////Act.
            var resposne = stubData.GetConferenceRoomList();

            ////Assert.
            var repsonseFirst = resposne.FirstOrDefault();
            if (repsonseFirst != null)
            {
                NUnitFramework.Assert.AreEqual(repsonseFirst.Id, 1);
            }
        }
    }
}
