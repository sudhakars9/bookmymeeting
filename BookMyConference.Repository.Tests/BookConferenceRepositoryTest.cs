﻿//-----------------------------------------------------------------------
// <copyright file="BookConferenceRepositoryTest.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entity;
    using Contract;
    using MongoDB.Driver;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Description for BookConferenceRepositoryTest
    /// </summary>
    [TestFixture]
    public class BookConferenceRepositoryTest
    {
        /// <summary>
        /// The conference repository
        /// </summary>
        private readonly IBookConferenceRepository conferenceRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BookConferenceRepositoryTest" /> class.
        /// </summary>
        public BookConferenceRepositoryTest()
        {
            var connectionManager = new Mock<IDatabaseConnectionManager>();
            var stubDataProvider = new Mock<IStubDataProvider>();
            stubDataProvider.Setup(x => x.GetConferenceRoomList()).Returns(this.getAvaiableConferenceRooms());
            var mongo = new MongoDatabase(new MongoServer(new MongoServerSettings()), "My", new MongoDatabaseSettings());
            connectionManager.Setup(x => x.ConnectToDatabase()).Returns(mongo);
            this.conferenceRepository = new BookConferenceRepository(connectionManager.Object, stubDataProvider.Object);
        }

        /// <summary>
        /// Test_s the Test_Save_Single_MeetingInstance_Failed.
        /// </summary>
        [Test]
        public void Test_Save_Single_MeetingInstance_Failed()
        {
            var testResult = this.conferenceRepository.SaveMeetingInfo(this.GetRoomDetails());
            Assert.IsFalse(testResult);
        }

        /// <summary>
        /// Test_s the check_ if_ single_ meeting instance_ exists.
        /// </summary>
        [Test]
        public void Test_Check_If_Single_MeetingInstance_Exists()
        {
            var testResult = this.conferenceRepository.CheckExistsMeetingInfo(this.GetRoomDetails());
            Assert.IsTrue(testResult);
        }

        /// <summary>
        /// Test_s the check_ if_ single_ meeting instance_ not_ exists.
        /// </summary>
        [Test]
        public void Test_Check_If_Single_MeetingInstance_Not_Exists()
        {
            var conferenceRoom = this.GetRoomDetails();
            conferenceRoom.BookingRoomLocation = "1F ODC1 Conf1";
            conferenceRoom.StartDateTime = DateTime.Parse("2015-01-08 14:40:52");
            conferenceRoom.EndDateTime = DateTime.Parse("2015-01-08 16:40:52");

            var testResult = this.conferenceRepository.CheckExistsMeetingInfo(conferenceRoom);
            Assert.IsFalse(testResult);
        }

        /// <summary>
        /// Test_s the count_ retrieved_ booked meeting_ location_ based.
        /// </summary>
        [Test]
        public void Test_Count_Retrived_BookedMeeting_Location_Based()
        {
            Assert.GreaterOrEqual(this.conferenceRepository.RetriveBookedMeetingInfo("2F ODC2 Conf1").Count(), 1);
        }

        /// <summary>
        /// Test_Available_Meeting_Rooms_Count_Retrived_Based_On_StartAndEndDate
        /// </summary>
        [Test]
        public void Test_Available_Meeting_Rooms_Count_Retrived_Based_On_StartAndEndDate()
        {
            // Add a record with room : 2F ODC2 Conf1  and current time with 1 hour duration
            this.conferenceRepository.SaveMeetingInfo(this.GetRoomDetails());
            var startTime = DateTime.Now.AddMinutes(5);
            var endTime = DateTime.Now.AddMinutes(30);

            var availableRoomList = this.conferenceRepository.GetAvailableRoomsList(startTime, endTime);
            var conferenceRoomsList = availableRoomList as ConferenceRoom[] ?? availableRoomList.ToArray();
            Assert.AreEqual(conferenceRoomsList.Where(x => x.Name == "2F ODC2 Conf1").ToList().Count(), 0);
            Assert.AreEqual(conferenceRoomsList.Select(x => x.Name != "2F ODC2 Conf1").Any(), true);
        }

        /// <summary>
        /// Gets the room details.
        /// </summary>
        /// <returns>Will return the conference details.</returns>
        private ConferenceRoomDetails GetRoomDetails()
        {
            return new ConferenceRoomDetails
            {
                StartDateTime = DateTime.Now,
                EndDateTime = DateTime.Now.AddHours(1),
                TypeOfBooking = "FullDay",
                BookingRoomLocation = "2F ODC2 Conf1",
                Employee = new Employee() { EmailId = "test36@tcs.com", EmployeeName = "Vin test", MobileNumber = "1234567890" },
                PurposeOfBooking = "Training"
            };
        }

        /// <summary>
        /// Gets conference rooms list.
        /// </summary>
        /// <returns>Will return the conference stub data list.</returns>
        private IEnumerable<ConferenceRoom> getAvaiableConferenceRooms()
        {
            var roomsList = new List<ConferenceRoom>
            {
                new ConferenceRoom {Id = 1, Name = "2F ODC2 Conf1", Projector = true, Lync = false},
                new ConferenceRoom {Id = 1, Name = "1F ODC2 Conf2", Projector = true, Lync = false},
                new ConferenceRoom {Id = 1, Name = "3F ODC3 Conf1", Projector = true, Lync = false}
            };
            return roomsList;
        }
    }
}
