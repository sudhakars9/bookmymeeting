﻿//-----------------------------------------------------------------------
// <copyright file="DatabaseConnectionManagerTest.cs" company="Aviva">
//    Copyright (c) Aviva. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Tests
{
    using Contract;
    using MongoDB.Driver;
    using NUnit.Framework;

    /// <summary>
    /// Tests Database Connection manager.
    /// </summary>
    [TestFixture]
    public class DatabaseConnectionManagerTest
    {
        /// <summary>
        /// Tests Database Connection.
        /// </summary>
        [Test]
        public void TestConnectToDatabase()
        {
            ////Setup.
            var mock = new Moq.Mock<IConfigurationWrapper>();
            mock.Setup(x => x.GetConnectionString("MongoDBConnection"))
                .Returns("mongodb://localhost1/ConferenceRoomInfo");
            DatabaseConnectionManager connectionManager = new DatabaseConnectionManager(mock.Object);
            ////Act.
            var databse = connectionManager.ConnectToDatabase() as MongoDatabase;
            ////Assert.
            if (databse != null)
            {
                Assert.AreEqual(databse.Name, "ConferenceRoomInfo");
            }
        }
    }
}
