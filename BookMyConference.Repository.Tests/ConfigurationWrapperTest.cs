﻿//-----------------------------------------------------------------------
// <copyright file="ConfigurationWrapperTest.cs" company="Aviva">
//    Copyright (c) Aviva. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// Test Class for StubData
    /// </summary>
    [TestFixture]
    public class ConfigurationWrapperTest
    {
        /// <summary>
        /// Tests Test_GetAppSettingsValue operation.
        /// </summary>
        [Test]
        public void Test_GetAppSettingsValue()
        {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            Assert.AreEqual(wrapper.GetAppSettingsValue("PreserveLoginUrl"), "true");
        }

        /// <summary>
        /// Tests Test_GetConnectionStringValue operation.
        /// </summary>
        [Test]
        public void Test_GetConnectionStringValue()
        {
            ConfigurationWrapper wrapper = new ConfigurationWrapper();
            var connectionString = wrapper.GetConnectionString("DefaultConnection");
            Assert.AreEqual(connectionString, "Hello");
        }
    }
}
