﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceControllerTest.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Web.Tests.Controllers
{
    using System;
    using System.Web.Mvc;
    using NUnit.Framework;
    using Web.Controllers;

    /// <summary>
    /// Test class for ConferenceController.
    /// </summary>
    [TestFixture]
    public class ConferenceControllerTest
    {
        /// <summary>
        /// Tests Index.
        /// </summary>
        [Test]
        public void Test_Index()
        {
            ////Setup
            ConferenceController controller = new ConferenceController();
            ////Act.
            var result = controller.Index();
            Type typeOfObject = result.GetType();
            ////Assert.
            Assert.AreEqual(typeOfObject.Name, typeof(ViewResult).Name);
        }

        /// <summary>
        /// Tests Calendar view.
        /// </summary>
        [Test]
        public void Test_View_Calendar()
        {
            ////Setup
            ConferenceController controller = new ConferenceController();
            ////Act.
            var result = controller.ViewCalendar();
            Type typeOfObject = result.GetType();
            ////Assert.
            Assert.AreEqual(typeOfObject.Name, typeof(ViewResult).Name);
        }
    }
}
