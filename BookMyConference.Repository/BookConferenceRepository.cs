﻿//-----------------------------------------------------------------------
// <copyright file="BookConferenceRepository.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Contract;
    using Entity;
    using MongoDB.Driver;
    using MongoDB.Driver.Linq;

    /// <summary>
    /// Class BookConferenceRepository
    /// </summary>
    public class BookConferenceRepository : IBookConferenceRepository
    {
        /// <summary>
        /// The collection
        /// </summary>
        private readonly MongoCollection collection;

        private readonly IStubDataProvider stubDataProvider;
        ////private readonly MongoDatabase database = new MongoClient(ConnectionString).GetServer().GetDatabase("ConferenceRoomInfo");

        /// <summary>
        /// Initializes a new instance of the <see cref="BookConferenceRepository" /> class.
        /// </summary>
        /// <param name="databaseConnectionManager">Database connection manager.</param>
        /// <param name="stubDataProvider"></param>
        public BookConferenceRepository(IDatabaseConnectionManager databaseConnectionManager, IStubDataProvider stubDataProvider)
        {
            var database = databaseConnectionManager.ConnectToDatabase();
            this.collection = database.GetCollection<ConferenceRoomDetails>("ConferenceRoomBookingDB");
            this.stubDataProvider = stubDataProvider;
        }

        /// <summary>
        /// Save the data into Mongo DB
        /// </summary>
        /// <param name="bookingInfo">Contains the booking Info.</param>
        /// <returns>
        /// will return the boolean value of saving meeting info in Database.
        /// </returns>
        public bool SaveMeetingInfo(ConferenceRoomDetails bookingInfo)
        {
            try
            {
                //// If NOT data exists
                if (this.CheckExistsMeetingInfo(bookingInfo))
                {
                    return false;
                }

                this.collection.Insert(bookingInfo);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Check if data exists in Mongo DB
        /// </summary>
        /// <param name="bookingInfo">Contains the booking Info.</param>
        /// <returns>
        /// Will return the boolean value to check the Meeting exist or not.
        /// </returns>
        public bool CheckExistsMeetingInfo(ConferenceRoomDetails bookingInfo)
        {
            var checkExistsMeetingInfo = false;
            var recordCnt = this.collection.AsQueryable<ConferenceRoomDetails>()
                .Where(x => x.BookingRoomLocation == bookingInfo.BookingRoomLocation)
                //// Compare new booking date falls between existing records dates 
                .Where(x => (x.StartDateTime <= bookingInfo.StartDateTime && x.EndDateTime > bookingInfo.StartDateTime)
                            || (x.StartDateTime >= bookingInfo.StartDateTime && x.StartDateTime <= bookingInfo.EndDateTime))
                .AsQueryable().Count();

            if (recordCnt > 0)
            {
                checkExistsMeetingInfo = true;
            }

            return checkExistsMeetingInfo;
        }

        /// <summary>
        /// Retrieve filtered data from DB
        /// </summary>
        /// <param name="bookingRoomLocation">The booking room location.</param>
        /// <returns>
        /// Will return the collection of booking history details.
        /// </returns>
        public IEnumerable<ConferenceRoomDetails> RetriveBookedMeetingInfo(string bookingRoomLocation)
        {
            if (string.IsNullOrEmpty(bookingRoomLocation))
            {
                return null;
            }

            var conferenceList = this.collection.FindAllAs<ConferenceRoomDetails>()
                .Where(x => x.BookingRoomLocation == bookingRoomLocation)
                .Where(x => x.StartDateTime.Date >= DateTime.Now.Date).AsEnumerable();

            return conferenceList;
        }

        /// <summary>
        /// Retrieve available rooms list filtered by mongo db data.
        /// </summary>
        /// <param name="startTime">start date and time value.</param>
        /// <param name="endTime">end date and time value.</param>
        /// <returns>
        /// Will return the collection of available rooms list.
        /// </returns>
        public IEnumerable<ConferenceRoom> GetAvailableRoomsList(DateTime startTime, DateTime endTime)
        {
            var availableRoomsList = stubDataProvider.GetConferenceRoomList();
            var bookedConferenceRoomsBookedData = this.collection.FindAllAs<ConferenceRoomDetails>()
                .Where(x => (x.StartDateTime <= startTime && x.EndDateTime > startTime)
                            || (x.StartDateTime >= startTime && x.StartDateTime <= endTime))
                .AsQueryable();

            if (bookedConferenceRoomsBookedData.Any())
            {
                foreach (var bookedRecord in bookedConferenceRoomsBookedData)
                {
                    availableRoomsList = availableRoomsList.Where(x => x.Name != bookedRecord.BookingRoomLocation).ToList();
                }
                return availableRoomsList;
            }
            else
            {
                return stubDataProvider.GetConferenceRoomList();
            }
        }
    }
}
