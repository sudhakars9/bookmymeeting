﻿//-----------------------------------------------------------------------
// <copyright file="ConfigurationWrapper.cs" company="TCS">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository
{
    using System.Configuration;
    using Contract;

    /// <summary>
    /// This is configuration wrapper
    /// </summary>
    public class ConfigurationWrapper : IConfigurationWrapper
    {
        /// <summary>
        /// Returns value from configuration file.
        /// </summary>
        /// <param name="key">Key name.</param>
        /// <returns>
        /// Returns Value.
        /// </returns>
        public string GetAppSettingsValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Get Connection String.
        /// </summary>
        /// <param name="key">Key name.</param>
        /// <returns>
        /// Returns connection string
        /// </returns>
        public string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
    }
}
