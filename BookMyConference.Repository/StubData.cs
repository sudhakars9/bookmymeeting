﻿//-----------------------------------------------------------------------
// <copyright file="StubData.cs" company="Aviva">
//    Copyright (c) Aviva. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using Contract;
    using Entity;

    /// <summary>
    /// Class For meetingRoom Stub Data.
    /// </summary>
    public class StubData : IStubDataProvider
    {
        /// <summary>
        /// wrapper interface for configuration manager.
        /// </summary>
        private readonly IConfigurationWrapper wrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StubData" /> class.
        /// </summary>
        /// <param name="wrapper">configuration manager wrapper.</param>
        public StubData(IConfigurationWrapper wrapper)
        {
            this.wrapper = wrapper;
        }

        /// <summary>
        /// Method to return conference rooms.
        /// </summary>
        /// <returns>List of Conference rooms.</returns>
        public IEnumerable<ConferenceRoom> GetConferenceRoomList()
        {
            List<ConferenceRoom> result = new List<ConferenceRoom>();
            string xmlPath;
            xmlPath = Path.Combine(AppDomain.CurrentDomain.RelativeSearchPath, this.wrapper.GetAppSettingsValue("Conference_Room_Data_Key"));
            var descendendts = XDocument.Load(xmlPath).Descendants("ConferenceRoom");
            var conferenceRooms = from item in descendendts
                                  let elementId = item.Element("Id")
                                  where elementId != null
                                  let elementName = item.Element("Name")
                                  where elementName != null
                                  let elementProjector = item.Element("Projector")
                                  where elementProjector != null
                                  let elementLync = item.Element("Lync")
                                  where elementLync != null
                                  select new
                                  {
                                      Code = elementId.Value,
                                      roomName = elementName.Value,
                                      roomProjectorAvailability = elementProjector.Value,
                                      roomLyncAvailability = elementLync.Value
                                  };

            foreach (var item in conferenceRooms)
            {
                int id;
                bool projector;
                bool lync;
                bool convertedToInt = int.TryParse(item.Code, out id);
                bool convertedToBoolOfProjector = bool.TryParse(item.roomProjectorAvailability, out projector);
                bool convertedToBoolOfLync = bool.TryParse(item.roomLyncAvailability, out lync);
                result.Add(new ConferenceRoom
                {
                    Id = convertedToInt ? id : 0,
                    Name = item.roomName,
                    Projector = convertedToBoolOfProjector ? projector : false,
                    Lync = convertedToBoolOfLync ? lync : false
                });
            }

            return result.OrderBy(room => room.Name);
        }
    }
}
