﻿//-----------------------------------------------------------------------
// <copyright file="DatabaseConnectionManager.cs" company="Aviva">
//    Copyright (c) Aviva. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.Repository
{
    using System.Linq;
    using Contract;
    using MongoDB.Driver;

    /// <summary>
    /// Class DatabaseConnectionManager
    /// </summary>
    public class DatabaseConnectionManager : IDatabaseConnectionManager
    {
        /// <summary>
        /// Configuration Wrapper.
        /// </summary>
        private readonly IConfigurationWrapper configurationWrapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseConnectionManager" /> class.
        /// </summary>
        /// <param name="configurationWrapper">Configuration wrapper.</param>
        public DatabaseConnectionManager(IConfigurationWrapper configurationWrapper)
        {
            this.configurationWrapper = configurationWrapper;
        }

        /// <summary>
        /// Connects to Database.
        /// </summary>
        /// <returns>Database handler.</returns>
        public dynamic ConnectToDatabase()
        {
            var databaseConnectionString = this.configurationWrapper.GetConnectionString("MongoDBConnection");
            var client = new MongoClient(databaseConnectionString);
            var server = client.GetServer();
            var arrayOfDataElements = databaseConnectionString.Split('/');
            var databaseName = arrayOfDataElements[arrayOfDataElements.Count() - 1];
            var database = server.GetDatabase(databaseName);
            return database;
        }
    }
}
