﻿//-----------------------------------------------------------------------
// <copyright file="ConferenceController.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.WebApi.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using Entity;
    using Repository.Contract;

    /// <summary>
    /// Class for Conference controller.
    /// </summary>
    public class ConferenceController : ApiController
    {
        /// <summary>
        /// Provider for stub data.
        /// </summary>
        private readonly IStubDataProvider provider;

        /// <summary>
        /// The _conference repository
        /// </summary>
        private readonly IBookConferenceRepository conferenceRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConferenceController"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="conferenceRepository">The conference repository.</param>
        public ConferenceController(IStubDataProvider provider, IBookConferenceRepository conferenceRepository)
        {
            this.provider = provider;
            this.conferenceRepository = conferenceRepository;
        }

        /// <summary>
        /// Gets the conference room list.
        /// </summary>
        /// <returns>Will return the status of the API service response.</returns>
        public HttpResponseMessage GetConferenceRoomList()
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.provider.GetConferenceRoomList());
        }

        /// <summary>
        /// Posts the save conference data.
        /// </summary>
        /// <param name="bookingData">My data.</param>
        /// <returns>Will return the status of the service response.</returns>
        [HttpPost]
        public HttpResponseMessage PostSaveConferenceData(ConferenceRoomDetails bookingData)
        {
            TimeSpan ss = (bookingData.EndDateTime - bookingData.StartDateTime);
            bookingData.MeetingDurationTime = string.Format("{0:%d} day(s)-{0:%h}:{0:%m}", ss);

            bookingData.MeetingDurationInDate = string.Format("{0} - {1}", bookingData.StartDateTime, bookingData.EndDateTime);

            return Request.CreateResponse(HttpStatusCode.OK, this.conferenceRepository.SaveMeetingInfo(bookingData));
        }

        /// <summary>
        /// Gets the check room availability.
        /// </summary>
        /// <param name="startTime">The start time.</param>
        /// <param name="endTime">The end time.</param>
        /// <param name="conferenceRoom">The conference room.</param>
        /// <returns>Will return the status of the service response.</returns>
        [HttpGet]
        public HttpResponseMessage GetCheckRoomAvailability(DateTime startTime, DateTime endTime, string conferenceRoom)
        {
            var searchEntity = new ConferenceRoomDetails();
            searchEntity.StartDateTime = startTime;
            searchEntity.EndDateTime = endTime;
            searchEntity.BookingRoomLocation = conferenceRoom;
            var isAvailable = !this.conferenceRepository.CheckExistsMeetingInfo(searchEntity);
            return Request.CreateResponse(HttpStatusCode.OK, isAvailable);
        }

        /// <summary>
        /// Gets the booked meeting information.
        /// </summary>
        /// <param name="conferenceRoom">The conference room.</param>
        /// <returns>Will return the list of meeting information</returns>
        [HttpGet]
        public HttpResponseMessage GetBookedMeetingInformation(string conferenceRoom)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.conferenceRepository.RetriveBookedMeetingInfo(conferenceRoom));
        }

        /// <summary>
        /// Gets available meeting room information.
        /// </summary>
        /// <param name="startTime">start date and time value.</param>
        /// <param name="endTime">end date and time value.</param>
        /// <returns>Will return the list of meeting information</returns>
        [HttpGet]
        public HttpResponseMessage GetAvailableRooms(DateTime startTime, DateTime endTime)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.conferenceRepository.GetAvailableRoomsList(startTime,endTime));
        }
    }
}
