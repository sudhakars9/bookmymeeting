//-----------------------------------------------------------------------
// <copyright file="IoC.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace BookMyConference.WebApi.DependencyResolution 
{
    using Repository;
    using Repository.Contract;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// Structure map IoC class.
    /// </summary>
    public static class IoC 
    {
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <returns>Object Container.</returns>
        public static IContainer Initialize() 
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
            ////                x.For<IExample>().Use<Example>();
                            x.For<IStubDataProvider>().Use<StubData>();
                            x.For<IBookConferenceRepository>().Use<BookConferenceRepository>();
                            x.For<IDatabaseConnectionManager>().Use<DatabaseConnectionManager>();
                            x.For<IConfigurationWrapper>().Use<ConfigurationWrapper>();
                        });
            return ObjectFactory.Container;
        }
    }
}