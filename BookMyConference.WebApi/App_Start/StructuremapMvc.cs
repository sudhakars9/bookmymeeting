//-----------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Aviva">
//    Copyright (c) TCS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Web.Http;
using System.Web.Mvc;
using BookMyConference.WebApi;
using BookMyConference.WebApi.DependencyResolution;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof(StructuremapMvc), "Start")]

namespace BookMyConference.WebApi
{
    /// <summary>
    /// Class for Structure map.
    /// </summary>
    public static class StructuremapMvc
    {
        /// <summary>
        /// Starts this instance.
        /// </summary>
        public static void Start()
        {
            var container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}